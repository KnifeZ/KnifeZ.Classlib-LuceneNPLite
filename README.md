# KnifeZ.Classlib-LuceneNPLite

### 项目介绍
Lucene.net+Pangu分词实现站内全文检索功能

### 软件架构
Lucene.Net 3.0.3

JiebaNet.Analyser 1.0.3 

JiebaNet.Segmenter 1.0.6

PanGu 2.3.1


### 安装教程

1. 克隆本项目
2. release模式下生成项目，找到bin/release下的`KnifeZ.ClassLib.LuceneNP.dll` dll文件，复制到想要引用的项目文件夹内
3. 在目标项目的 依赖项/引用 右键添加项目引用，把dll文件引入到项目中。

### 使用说明

```` C#
using KnifeZ.ClassLib.LuceneNP;
using KnifeZ.ClassLib.LuceneNP.JiebaVersion;

        //创建索引，Infos类型可改，
        public static string CreatedIndex(List<Infos> list, string domain)
        {
            var currList = new List<LiteNewsModel>();
            if (list.Any())
            {
                foreach (var item in list)
                {
                    var tmp = new LiteNewsModel()
                    {
                        BillCode = item.FK_InfoKeys_UNID.ToString(),
                        Content = item.Info ?? "",
                        Title = item.Title,
                        Url = item.Keyword,
                        Abstract = item.Info.ToString(),
                        Time = DateTime.Now
                    };
                    //清除内容中的Html代码，推荐添加
                    //tmp.Content =KnifeZ.Unity.KnifeHelper.RemoveHtmlTags(tmp.Content,0);
                    currList.Add(tmp);
                }
            }
            var ret = SearchEngine.CreatedIndex(currList, domain);
            return ret;
        }
        //删除指定索引
        public static bool DeleteIndex(string billCode, string domain)
        {
            var index = new LNPIndex(domain);
            return index.Delete(billCode);
        }
        //删除全部索引
        public static bool DeleteAll(string domain)
        {
            var index = new LNPIndex(domain);
            return index.DeleteAll();
        }
        //添加索引 Content类型可改
        public static void AddIndex(Content model, string domain)
        {
            var index = new LNPIndex(domain);
            var liteModel = new LiteNewsModel()
            {
                BillCode = model.ID.ToString(),
                Content = model.Introduce,
                Title = model.Title,
                Url = model.Forecast,
                Abstract = model.Name.ToString(),
                Time = DateTime.Now
            };
            index.AddSingleIndex(liteModel);
        }
        //以Pangu分词的模式查询
        public static List<Infos> Query(string key, string domain, int top = 100)
        {
            var ret = new List<Infos>();
            var list = SearchEngine.QueryList(key, top, domain);
            if (list.Any())
            {
                foreach (var item in list)
                {
                    ret.Add(new Infos()
                    {
                        FK_InfoKeys_UNID = Convert.ToInt32(item.BillCode),
                        Info = item.Content,
                        Title = item.Title,
                        Keyword = item.Url
                    });
                }
            }
            return ret;
        }
        //按结巴分词模式查询
        public static List<Infos> QueryByJieba(string key)
        {
            var ret = new List<Infos>();
            var list = JiebaIndexEngine.Search(key);
            if (list.Any())
            {
                foreach (var item in list)
                {
                    ret.Add(new Infos()
                    {
                        FK_InfoKeys_UNID = Convert.ToInt32((item.BillCode == "" || item.BillCode.Length > 30) ? "0" : item.BillCode),
                        Info = item.Content,
                        Title = item.Title,
                        Keyword = item.Url
                    });
                }
            }
            return ret;
        }

````