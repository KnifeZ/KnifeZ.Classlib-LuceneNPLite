﻿using JiebaNet.Analyser;
using JiebaNet.Segmenter;
using JiebaNet.Segmenter.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnifeZ.ClassLib.LuceneNP.JiebaVersion
{
    public class WordMixByJieba
    {
        public static List<KeyValuePair<string, int>> CutAndCountKeys(string text, bool cutAll = false, bool hmm = true)
        {
            var seg = new JiebaSegmenter();
            var freqs = new Counter<string>(seg.Cut(text, cutAll, hmm));
            var ret = new List<KeyValuePair<string, int>>();

            foreach (var pair in freqs.MostCommon())
            {
                ret.Add(new KeyValuePair<string, int>(pair.Key, pair.Value));
            }
            return ret;
        }
        public static IEnumerable<string> Cut(string text, bool cutAll = false, bool hmm = true)
        {
            var seg = new JiebaSegmenter();
            var freqs = seg.Cut(text, cutAll, hmm);
            return freqs;
        }

        /// <summary>
        /// 获取text关键词
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static IEnumerable<string> GetKeywordsByTFIDF(string text)
        {
            var extractor = new TfidfExtractor();
            var keywords = extractor.ExtractTags(text,allowPos:Constants.NounAndVerbPos);
            return keywords;
        }
        public static IEnumerable<string> GetKeywordsByTextRank(string text)
        {
            var extractor = new TextRankExtractor();
            var keywords = extractor.ExtractTags(text);
            return keywords;
        }
    }
}
